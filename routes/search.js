module.exports = function (app) {
    app.post('/search', function (req, res) {
        res.render('search', { queryTerm: req.param('searchterm') });
    });
};