module.exports = function(app) {
    /**
     * Serves up the homepage
     */
    app.get('/', function (req, res) {
        res.render('home.html', {});
    });

    app.get('/about', function (req, res) {
        res.render('about.html', {});
    });

    app.get('/contact', function (req, res) {
        res.render('contact.html', {});
    })
};