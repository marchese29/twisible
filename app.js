/**
 * Module dependencies.
 */
var express = require('express');
var socketio = require('socket.io');
var twitter = require('ntwitter');
var swig = require('swig');

console.log('Configuring the server...');
process.openStdin();
var app = express();
var server = app.listen(80);
app.configure(function() {
    app.use(express.logger());
    app.engine('html', swig.renderFile);
    app.set('view engine', 'html');
    app.use(express.static(__dirname + '/public'));
    app.set('view cache', false);
    app.use(express.bodyParser());
});

console.log('Setting up twitter api...');
var twit = new twitter({
    consumer_key: 'u7duUI1NTFCsabH02F9kpA',
    consumer_secret: 'Qx92UUvpeybHC6OBER6faZc0IIjmbSNhuabrJN0gpI',
    access_token_key: '2332819196-FKKHPeRxsl84CihAfgPvsh3UJyUHiP04S7zbVSv',
    access_token_secret: 'wJEsWXBA6G7G4zZ7yZOulTahDIfdb2gAq7Awc0HhucVKS'
});

var searchTerms = [];
var searchCounts = [];

console.log('Configuring Web Sockets...');
var io = socketio.listen(server);
function set_stream_socks() {
    twit.stream('statuses/filter', { track: searchTerms }, function (stream) {
        stream.on('data', function (tweet) {
            for (var i = 0; i < searchTerms.length; i++) {
                if (tweet.text != undefined) {
                    if (tweet.text.toLowerCase().indexOf(searchTerms[i]) != -1) {
                        var context = {
                            text: tweet.text
                        };
                        console.log(tweet.text);
                        if (tweet.entities.media != undefined) {
                            console.log(tweet.entities.media[0]);
                            context['media_url'] = tweet.entities.media[0].media_url_https;
                        }
                        io.sockets.in(searchTerms[i]).emit('tweet', context);
                    }
                }
            }
        });

        stream.on('error', function (err) {
            console.log("ERROR!!!!! " + err);
        });
    });
}

io.sockets.on('connection', function (socket) {
    socket.on('query', function (data) {
        socket.query = data.query;
        socket.join(data.query);
        if (searchTerms.indexOf(data.query) == -1) {
            console.log("Pushing term " + data.query);
            searchTerms.push(data.query);
            searchCounts.push(1);

            set_stream_socks();
        } else {
            var index = searchTerms.indexOf(data.query);
            searchCounts[index]++;
        }
    });

    socket.on('disconnect', function () {
        socket.leave(socket.query);
        var index = searchTerms.indexOf(socket.query);
        searchCounts[index]--;
        if (searchCounts[index] < 1) {
            searchTerms.splice(index, 1);
            searchCounts.splice(index, 1);

            if (searchCounts.length > 0) {
                set_stream_socks();
            }
        }
    });
});

console.log('Configuring Routes...');
require('./routes/index')(app);
require('./routes/search')(app);

console.log('Configuring Interrupts...');
process.on('SIGINT', function () {
    console.log('\nGot SIGINT, exiting.');
    process.exit(0);
});
